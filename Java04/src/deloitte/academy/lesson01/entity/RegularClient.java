package deloitte.academy.lesson01.entity;

import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson01.logic.Hotel;
import deloitte.academy.lesson01.run.RunHotel;

/**
 * Class RegularClient extends to class Hotel
 * 
 * @author dmascott
 *
 */
public class RegularClient extends Hotel {

	/*
	 * The object Logger is initialized, it will obtain the method name as a value
	 */
	final Logger LOGGER = Logger.getLogger(RegularClient.class.getName());

	/*
	 * Constructor of superClass of the class RegularClient
	 */
	public RegularClient() {
		super();
	}

	/*
	 * Constructor of fields of the class RegularClient
	 */
	public RegularClient(int room, double cost, String client) {
		super(room, cost, client);
	}

	/*
	 * Execution of the method payment Multiplies the cost * the discount to the
	 * client Regular Client
	 */
	@Override
	public double payment() {
		double total = this.getCost() * TypesClients.RegularClient.getDiscount();
		return total;
	}

	/*
	 * Execution of the method checkIn When the room setted is found checks if is
	 * empty or occupied if its occupied sends a message of error otherwise it
	 * changes the status of the room and sends a succesful message
	 */
	@Override
	public String checkIn() {
		String registry = "";

		try {
			for (Rooms room : RunHotel.roomsList) {
				if (this.getRoom() == room.getIdRoom()) {
					if (room.isStatus() == true) {
						registry = "The room " + room.getIdRoom() + " is occupied";

					} else {
						room.setStatus(true);
						registry = "The user has been register succesfully";
						break;
					}
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return registry;
	}

	/*
	 * Execution of the method checkOut When the room setted is found checks if is
	 * empty or occupied if its occupied changes the status of the room and sends a
	 * succesful message otherwise sends an error message
	 */
	@Override
	public String checkOut() {
		String registry = "";

		try {
			for (Rooms room : RunHotel.roomsList) {
				if (this.getRoom() == room.getIdRoom()) {
					if (room.isStatus() == true) {
						room.setStatus(false);
						registry = "The user has been unregister succesfully";
						break;
					} else {
						registry = "The room " + room.getIdRoom() + " was not occupied";
					}
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return registry;
	}

}
