package deloitte.academy.lesson01.run;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import deloitte.academy.lesson01.entity.Employee;
import deloitte.academy.lesson01.entity.FrequentClient;
import deloitte.academy.lesson01.entity.RegularClient;
import deloitte.academy.lesson01.entity.Rooms;

public class RunHotel {

	/*
	 * A global Array with Rooms values is created
	 */
	public static final ArrayList<Rooms> roomsList = new ArrayList<Rooms>();

	/*
	 * The object Logger is initialized, it will obtain the method name as a value
	 */
	final static Logger LOGGER = Logger.getLogger(RunHotel.class.getName());

	public static void main(String[] args) {
		/*
		 * The objects room are instantiated
		 */
		Rooms room1 = new Rooms(1, false);
		Rooms room2 = new Rooms(2, false);
		Rooms room3 = new Rooms(3, false);
		Rooms room4 = new Rooms(4, false);
		Rooms room5 = new Rooms(5, false);

		/*
		 * The rooms are added to the ArrayList "roomsList"
		 */
		roomsList.add(room1);
		roomsList.add(room2);
		roomsList.add(room3);
		roomsList.add(room4);
		roomsList.add(room5);

		/*
		 * An object regularClient is instantiated
		 */
		RegularClient regularClient = new RegularClient(1, 100, "Kevyn Mascott");

		/*
		 * An object frequentClient is instantiated
		 */
		FrequentClient frequentClient = new FrequentClient(2, 100, "Dyan Mascott");

		/*
		 * An object employee is instantiated
		 */
		Employee employee = new Employee(3, 100, "Eddie");

		/*
		 * An object rooms is instantiated
		 */
		Rooms rooms = new Rooms();

		/*
		 * The method roomsStatus of the class Rooms is called to show the initial rooms
		 * available
		 */
		List[] vacants = rooms.roomsStatus(roomsList);
		System.out.println("Available Rooms");
		LOGGER.info("The empty rooms are: " + vacants[0]);
		LOGGER.info("The occupied rooms are: " + vacants[1]);

		/*
		 * The method checkIn of the class RegularClient is created
		 */
		System.out.println("\nCheckIn");
		LOGGER.info(regularClient.checkIn());
		LOGGER.info(frequentClient.checkIn());

		/*
		 * The method roomsStatus of the class Rooms is called to show the rooms
		 * available after a checkIn
		 */
		List[] vacants1 = rooms.roomsStatus(roomsList);
		System.out.println("\nAvailable Rooms after checkIn");
		LOGGER.info("The empty rooms are: " + vacants1[0]);
		LOGGER.info("The occupied rooms are: " + vacants1[1]);

		/*
		 * The method checkOut of the class RegularClient is created
		 */
		System.out.println("\nCheckOut");
		LOGGER.info(regularClient.checkOut());

		/*
		 * The method roomsStatus of the class Rooms is called to show the rooms
		 * available after the checkOut
		 */
		List[] vacants2 = rooms.roomsStatus(roomsList);
		System.out.println("\nAvailable Rooms after checkOut");
		LOGGER.info("The empty rooms are: " + vacants2[0]);
		LOGGER.info("The occupied rooms are: " + vacants2[1]);

		System.out.println("\nClient Price with Discount");
		/*
		 * The method payment of the class RegularClient is created
		 */
		LOGGER.info("Regular Client Price: " + regularClient.payment());

		/*
		 * The method payment of the class FrequentClient is created
		 */
		LOGGER.info("Frequent Client Price " + frequentClient.payment());

		/*
		 * The method payment of the class Employee is created
		 */
		LOGGER.info("Employee Price: " + employee.payment());

	}
}
