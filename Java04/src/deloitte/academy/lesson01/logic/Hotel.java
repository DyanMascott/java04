package deloitte.academy.lesson01.logic;

/**
 * Abstract class Hotel
 * 
 * @author dmascott
 *
 */
public abstract class Hotel {

	/*
	 * The variables are created
	 */
	private int room;
	private double cost;
	private String client;

	/**
	 * Superclass Constructor
	 */
	public Hotel() {
		super();
	}

	/**
	 * The constructor of the object Hotel is created
	 * 
	 * @param room   int value
	 * @param cost   double value
	 * @param client String Value
	 */
	public Hotel(int room, double cost, String client) {
		super();
		this.room = room;
		this.cost = cost;
		this.client = client;
	}

	/*
	 * Getters and Setters are created
	 */
	public int getRoom() {
		return room;
	}

	public void setRoom(int room) {
		this.room = room;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	/*
	 * Abstract method payment
	 */
	public abstract double payment();

	/*
	 * Abstract method checkIn
	 */
	public abstract String checkIn();

	/*
	 * Abstract method checkOut
	 */
	public abstract String checkOut();

}
