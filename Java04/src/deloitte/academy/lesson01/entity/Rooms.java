package deloitte.academy.lesson01.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Rooms {

	/*
	 * The variables are created
	 */
	private int idRoom;
	private boolean status;

	/**
	 * Superclass Constructor
	 */
	public Rooms() {
		super();
	}

	/**
	 * The constructor of the object Rooms is created
	 * 
	 * @param idRoom int value
	 * @param status boolean value
	 */
	public Rooms(int idRoom, boolean status) {
		super();
		this.idRoom = idRoom;
		this.status = status;
	}

	/*
	 * Getters and Setters are created
	 */
	public int getIdRoom() {
		return idRoom;
	}

	public void setIdRoom(int idRoom) {
		this.idRoom = idRoom;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	/*
	 * The object Logger is initialized, it will obtain the method name as a value
	 */
	final Logger LOGGER = Logger.getLogger(Employee.class.getName());

	/**
	 * Method roomStatus
	 * 
	 * @param rooms An array with values of the object Rooms
	 * @return a List with int values
	 * 
	 *         Creates a list with two inner lists, occupied and empty, filled with
	 *         the array of the parameter
	 */
	public List[] roomsStatus(ArrayList<Rooms> rooms) {
		List<Integer> occupied = new ArrayList();
		List<Integer> empty = new ArrayList();

		try {
			for (Rooms vals : rooms) {
				if (vals.isStatus() == true) {
					occupied.add(vals.getIdRoom());
				} else {
					empty.add(vals.getIdRoom());
				}
			}

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return new List[] { empty, occupied };
	}

}
