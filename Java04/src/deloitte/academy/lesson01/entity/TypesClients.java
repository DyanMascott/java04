package deloitte.academy.lesson01.entity;

/**
 * Enum TypesClients
 * 
 * @author dmascott
 *
 */
public enum TypesClients {
	/*
	 * The Definition of the different types of clients
	 */
	RegularClient(1), FrequentClient(.70), Worker(.50);

	/*
	 * Constructor of the enum
	 */
	private TypesClients(double discount) {
		this.discount = discount;
	}

	/*
	 * A property discount for the client types
	 */
	private double discount;

	/*
	 * Getter and setter of the property discount from typesClients
	 */
	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

}
